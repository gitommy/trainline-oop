# Standard libraries
import os
from pprint import pprint as pp  # PrettyPrint
# from collections import OrderedDict  # Maybe used in future
# Non-standard libraries (pip install them!)
from bs4 import BeautifulSoup  # Requires 'pip install lxml'
# Custom libraries
from trainline import Tracker
from trainline import UndergroundStation
from trainline import UndergroundLine
import Task2Functions


tracker = Tracker()
###############################################################################
# Step 1 - Parse Stations.xml                                                 #
###############################################################################
stationsXmlFilePath = r".\DataFiles\Stations\Stations.xml"

print("Parsing Stations from file: {}\n".format(stationsXmlFilePath))
Task2Functions.parseStationsXml(stationsXmlFilePath, UndergroundStation,
                                BeautifulSoup, tracker)

# pp(Station.stationsDict)
# print(tracker.stationDict)

###############################################################################
# Step 2 - Parse JourneyTimes directory                                       #
###############################################################################
journeyTimesPath = r".\DataFiles\JourneyTimes"

print("Getting files from directory: {}\n".format(journeyTimesPath))
listOfFiles = Task2Functions.getListOfFilesFromDirectory(journeyTimesPath, os)
print("Found the following files: {}\n".format(listOfFiles))

print("Parsing JourneyTimes files...\n")
Task2Functions.parseJourneyTimesDirectory(journeyTimesPath, listOfFiles,
                                          BeautifulSoup, os, UndergroundLine,
                                          tracker)

# print(tracker.lineDict)
# for line in tracker._lineDict:
#     print(line)
#     pp(tracker.lineDict[line].journeyTimes, indent=4)
#     pp(tracker._lineDict[line].journeyTimes, indent=4)


###############################################################################
# Step 3 - Process JourneyTimes data into a usable structure                  #
###############################################################################
print("Processing complete JourneyTimes for each line...")
Task2Functions.processJourneyTimes(tracker)
