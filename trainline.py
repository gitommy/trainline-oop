class Tracker():
    """This class is used to keep track of, and store instances
    of other classes. Dictionaries are in the format
    {<name>: <instance of a class>"""
    def __init__(self):
        self._stationDict = {}
        self._lineDict = {}


class UndergroundStation():
    """This class is used to store details of a station, the dictionary
    stationsDict will store key:value as <station name>:<station object>

    Parameters
    ----------
    name        : srt
        Name of the station
    address     : srt
        Address of the station
    coordinates : str
        Coordinates of the station"""

    # stationsDict = {}  # Used to store the station objects

    def __init__(self, name, address, coordinates):
        self.name = name
        self.address = address
        self.coordinates = coordinates

    def printDetails(self):
        print("{}\n{}\n{}\n\n".format(self.name, self.address,
                                      self.coordinates))


class UndergroundLine():

    # Create a dictionary to store the UndergroundLine objects
    # format {line: UndergroundLine instance}
    # lineDict = {}

    def __init__(self):
        # Dictionary to store the journey times from the JourneyTimes
        # directory. This infomation is the the time between each station
        # going "forwards" on the line. format is {<from>: {<to>: <time>}}
        self.journeyTimes = {}
        # List to store the station order
        self.stationOrder = []
        # Dictionary to store the complete line journey times so you can
        # calculate the time to reach any station from any other station
        # on that line
        self.completeJourneyTimes = {}

    def printJourneyTimes(self):
        print("{}\n".format(self.journeyTimes))

    def printStationOrder(self):
        print("{}\n".format(self.stationOrder))
