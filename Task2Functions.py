###############################################################################
# Miscellaneous functions                                                     #
###############################################################################
def formatString(inputStr: str):
    """Function to format a string in various ways and return the formatted
    string.
    Parameters
    ----------
    inputStr : str
        An input string
    returns
    -------
    str : str
        The formatted string"""

    inputStr = inputStr.replace("Station", "")
    inputStr = inputStr.replace("&", "and")
    inputStr = inputStr.strip()
    return inputStr


def getListOfFilesFromDirectory(directory: str, os: object):
    """Function to search a directory and return a list of found files
    Parameters
    ----------
    directory : str
        Asbolute or relative path to a directory
    os : object
        The OS module
    Returns
    -------
    list
        A list containing all found files"""

    listOfFiles = []

    for f in os.listdir(directory):
        # print("Directory is: {}".format(directory))
        filePath = os.path.join(directory, f)
        # print("Full file path is: {}".format(filePath))
        if os.path.isfile(filePath):
            listOfFiles.append(f)
    return listOfFiles


###############################################################################
# Step 1 - Parse Stations.xml                                                 #
###############################################################################
def parseStationsXml(filepath: str, station: object, BeautifulSoup: object,
                     Tracker: object):
    """Function that will parse the Stations.xml file and populate stationsDict
    in the stations object
    parameters
    ----------
    filepath : str
        The relative or absolute filepath to Stations.xml
    station : object
        The station object (class)
    BeautifulSoup : object
        The BeautifulSoup object (class)"""

    # Open XML file and create soup
    with open(filepath, "r") as fp:
        soup = BeautifulSoup(fp, "xml")

    # Parse soup for station details
    for st in soup.find_all("Placemark"):
        nameRaw = st.find_all("name")
        addressRaw = st.find_all("description")
        coordinatesRaw = st.find_all("coordinates")

        name = formatString(nameRaw[0].get_text())
        address = formatString(addressRaw[0].get_text())
        coordinates = formatString(coordinatesRaw[0].get_text())

        # Create a station class instance and store in the Tracker class
        newStation = station(name, address, coordinates)
        Tracker._stationDict[name] = newStation


###############################################################################
# Step 2 - Parse JourneyTimes directory                                       #
###############################################################################
def parseJourneyTimesDirectory(directory: str, listOfFiles: list,
                               BeautifulSoup: object, os: object,
                               UndergroundLine: object, Tracker: object):
    """This function will parse the XML files that contain the RouteNodes and
    store the results of the parsing into an instance of the UndergroundLine
    class
    Parameters
    ----------
    directory : str
        The path to the directory containing the XML files
    listOfFiles : list
        A list of files to be parsed
    BeautifulSoup : object
        The BeautifulSoup module from bs4
    os : object
        The OS module
    UndergroundLine : object
        The UndergroundLine module from trainline
    Tracker : object
        The Tracker module from trainline"""
    # Dict to store the journey times in format {line: {from: {to: time}}}
    # lineJourneyTimes = {}
    # Dict to store the station order in format {line: [station name]} 
    # lineStationOrder = {}

    for fileName in listOfFiles:
        # Get the line name from the fileName
        lineName = formatString(fileName.split(".")[0])
        fullFilePath = os.path.join(directory, fileName)

        # Create an instance of the class UndergroundLine
        newUndergroundLine = UndergroundLine()

        # Open XML file and create soup
        with open(fullFilePath, "r") as fp:
            soup = BeautifulSoup(fp, "xml")

        # Used to ensure only the first fromStation is added to the
        # lineStationOrder[lineName] list
        firstStation = True

        # Parse soup for routeNode details (from, to and time)
        for routeNode in soup.find_all("RouteNode"):
            rawFromStation = routeNode.find_all("From")
            rawToStation = routeNode.find_all("To")
            rawRunTimeMinutes = routeNode.find_all("RunTimeMinutes")

            # Format the raw results into usable strings
            fromStation = formatString(rawFromStation[0].get_text())
            toStation = formatString(rawToStation[0].get_text())
            runTimeMinutes = formatString(rawRunTimeMinutes[0].get_text())

            # Add current parsed RouteNode to the dictionary
            newUndergroundLine.journeyTimes[fromStation] = {toStation: runTimeMinutes}

            # Add the "to" station to the stationOrder list, except for the
            # first time when add the "from" and "to" stations.
            if firstStation:
                newUndergroundLine.stationOrder.append(fromStation)
                newUndergroundLine.stationOrder.append(toStation)
            else:
                newUndergroundLine.stationOrder.append(toStation)
            firstStation = False

        # Add instance of the class UndergroundLine to
        # _lineDict for tracking
        Tracker._lineDict[lineName] = newUndergroundLine


###############################################################################
# Step 3 - Process JourneyTimes data into a usable structure                  #
###############################################################################
def processJourneyTimes(Tracker: object):
    for line in Tracker._lineDict:
        for fromStation in Tracker._lineDict[line].stationOrder:
            Tracker._lineDict[line].completeJourneyTimes[fromStation] = {}
            for toStation in Tracker._lineDict[line].stationOrder:
                if Tracker._lineDict[line].stationOrder.index(fromStation) \
                        < Tracker._lineDict[line].stationOrder.index(toStation):
                    currentStation = fromStation
                    journeyTime = 0
                    while currentStation != toStation:
                        for key, val in Tracker._lineDict[line].journeyTimes[currentStation].items():
                            currentStation = key
                            journeyTime = journeyTime + int(val)
                    # print("From: {}\nTo: {}\nTime: {}\n\n".format(fromStation, toStation, journeyTime))
                    Tracker._lineDict[line].completeJourneyTimes[fromStation][toStation] = journeyTime

        for fromStation in Tracker._lineDict[line].stationOrder:
            for toStation in Tracker._lineDict[line].stationOrder:
                if Tracker._lineDict[line].stationOrder.index(fromStation) \
                        > Tracker._lineDict[line].stationOrder.index(toStation):
                    Tracker._lineDict[line].completeJourneyTimes[fromStation][toStation] \
                        = Tracker._lineDict[line].completeJourneyTimes[toStation][fromStation]
